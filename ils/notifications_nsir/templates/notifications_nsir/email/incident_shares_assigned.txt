{% firstof share.responsible.first_name share.responsible.username %},

You were just assigned the following sharing action for incident #{{share.incident.pk}}.

{% if share.responsible.can_investigate %}Incident Page      : {{url}}{% endif %}
Assigned By 	   : {{share.assigned_by}}
Sharing Audience   : {{share.sharing_audience}}


=== Unsubscribe ===

Use the following link to unsubscribe from this incident:

{{unsubscribe}}
